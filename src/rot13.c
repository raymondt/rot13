/*
 * 	rot13.c
 *
 *  Created on: 16 July 2019
 *  Author: Raymond Thomson
 *  Email: raymond.thomson76@gmail.com
 *
 */

#define a (int)'a'
#define z (int)'z'
#define A (int)'A'
#define Z (int)'Z'

#include <stdio.h>
#include <unistd.h>
#include <limits.h>

int main () {
	char ch; 				// charactor value
	char buf[BUFSIZ]; 		// buffer of char
	int tc; 				// buffer return count

	while ((tc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < tc;i++) {
			ch = buf[i];
			if (ch >= a && ch <= z) {
				ch = (((ch-a)+13)%26)+a;
			} else if (ch >= A && ch <= Z) {
				ch = (((ch-A)+13)%26)+A;
			}
			buf[i] = ch;
		}
		write(1, buf, tc);
	}
	return 0;
}

