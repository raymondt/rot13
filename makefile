# Application : rot13 cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

rot13: $(SRC)rot13.o
	@echo 'Building and linking target: $@'
	$(CC) -o rot13 $(SRC)*.o $(LIBS)

local: rot13 clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./rot13 to run'

install: rot13 
	@echo 'Installing'	
	cp rot13 /usr/local/bin/rot13
	cp $(MAN)rot13 /usr/share/man/man1/rot13.1
	gzip /usr/share/man/man1/rot13.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f rot13
	@echo 'installed, type rot13 to run or man rot13 for the manual'

remove:
	rm -f rot13

uninstall:
	rm -f /usr/local/bin/rot13
	rm -f /usr/share/man/man1/rot13.1.gz
	@echo 'rot13 uninstalled.'

help:
	@echo 'Make options for rot13'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'














	