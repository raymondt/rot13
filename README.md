# rot13

This program is developed to perform a rot13 cypher from STDIN, and output the results to STDOUT.

Useful for performing the cypher on ASCII text files.

This program can be installed by downloading and extracting the source files, opening a terminal in the new directory and typing "sudo make install" or "make" to see the installation options.